import { renderSlot } from 'vue'

export default (element: any, include?: string[]) => {
  const patchRender = (render: any, args: any[]) => {
    const node = render(...args)

    if (typeof node === 'object' && node.children) {
      node.children = [
        renderSlot(args[0].$slots, 'before'),
        ...([] as any[]).concat(node.children),
        renderSlot(args[0].$slots, 'after'),
      ]
    }

    return node
  }

  const fakeApp = {
    use(component: any) {
      if (typeof component === 'object') {
        const { setup, render, name } = component
        if (include && include.indexOf(name as string) === -1) {
          return
        }

        if (setup) {
          component.setup = (_props: any, _obj: any) => {
            // eslint-disable-next-line no-shadow
            const render = setup(_props, _obj)
            return (...args: any) => {
              return patchRender(render, args)
            }
          }
        }

        if (render) {
          component.render = (...args: any) => {
            return patchRender(render, args)
          }
        }
      }
    },
  }

  element.install(fakeApp, null)
}
